Source: labwc
Section: x11
Priority: optional
Maintainer: Guilherme Puida Moreira <guilherme@puida.xyz>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 libcairo2-dev,
 libpango1.0-dev,
 libpng-dev,
 libseat-dev,
 libwayland-dev,
 libwlroots-dev (>= 0.16.0),
 libxkbcommon-dev,
 libxml2-dev,
 meson,
 pkgconf,
 scdoc,
 wayland-protocols,
Standards-Version: 4.6.2
Homepage: https://labwc.github.io
#Vcs-Browser: https://salsa.debian.org/debian/labwc
#Vcs-Git: https://salsa.debian.org/debian/labwc.git

Package: labwc
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Wayland window-stacking compositor
 Labwc is a wlroots-based window-stacking compositor for wayland, inspired
 by openbox.
 .
 It is light-weight and independent with a focus on simply stacking
 windows well and rendering some window decorations. It takes a
 no-bling/no-frills approach and says no to features such as animations. It
 relies on clients for panels, screenshots, wallpapers and so on to create
 a full desktop environment.
 .
 Labwc has no reliance on any particular Desktop Environment, Desktop Shell
 or session. Nor does it depend on any UI toolkits such as Qt or GTK.
